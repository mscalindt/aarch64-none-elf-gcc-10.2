/* Generated automatically. */
static const char configuration_arguments[] = "/tmp/dgboter/bbs/moonshot-dsg-05--aarch64/buildbot/aarch64-none-linux-gnu--aarch64-none-elf/build/src/gcc/configure --target=aarch64-none-elf --prefix=/tmp/dgboter/bbs/moonshot-dsg-05--aarch64/buildbot/aarch64-none-linux-gnu--aarch64-none-elf/build/build-aarch64-none-elf/install// --with-gmp=/tmp/dgboter/bbs/moonshot-dsg-05--aarch64/buildbot/aarch64-none-linux-gnu--aarch64-none-elf/build/build-aarch64-none-elf/host-tools --with-mpfr=/tmp/dgboter/bbs/moonshot-dsg-05--aarch64/buildbot/aarch64-none-linux-gnu--aarch64-none-elf/build/build-aarch64-none-elf/host-tools --with-mpc=/tmp/dgboter/bbs/moonshot-dsg-05--aarch64/buildbot/aarch64-none-linux-gnu--aarch64-none-elf/build/build-aarch64-none-elf/host-tools --with-isl=/tmp/dgboter/bbs/moonshot-dsg-05--aarch64/buildbot/aarch64-none-linux-gnu--aarch64-none-elf/build/build-aarch64-none-elf/host-tools --disable-shared --disable-nls --disable-threads --disable-tls --enable-checking=release --enable-languages=c,c++,fortran --with-newlib --with-pkgversion='GNU Toolchain for the A-profile Architecture 10.2-2020.11 (arm-10.16)' --with-bugurl=https://bugs.linaro.org/";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { NULL, NULL} };
